module.exports = [
  {
    text: 'Programming',
    ariaLabel: 'Languages',
    items: [
      {
        text: 'Python',
        items: [
          {
            text: 'Python 3.x',
            link: '/python/'
          },
          {
            text: 'Django',
            link: '/python/django.html'
          }
        ]
      },
      {
        text: 'Rust',
        items: [
          {
            text: 'Learning Rust',
            link: '/rust/'
          }
        ]
      },
      {
        text: 'Shell/Terminal',
        items: [
          {
            text: 'Bash',
            link: '/shell/bash.html'
          },
          {
            text: 'Zsh',
            link: '/shell/zsh.html'
          }
        ]
      }
    ]
  },
  {
    text: 'Administration',
    ariaLabel: 'Administration',
    items: [
      {
        text: 'Linux',
        items: [
          {
            text: 'Python 3.x',
            link: '/python/'
          },
        ]
      },
      {
        text: 'Containers',
        items: [
          {
            text: 'Docker',
            link: '/containers/docker/'
          },
          {
            text: 'K8S',
            link: '/containers/k8s/'
          }
        ]
      },
      {
        text: 'Workstation',
        items: [
          {
            text: 'Arch',
            link: '/workstation/arch.md'
          }
        ]
      }
    ]
  }
]
