const { fs, path } = require("@vuepress/shared-utils");

module.exports = ctx => ({
  dest: "dist",
  head: [
    ["link", { rel: "icon", href: `/icons/favicon-96x96.png` }],
    ["link", { rel: "manifest", href: "/icons/manifest.json" }],
    ["meta", { name: "theme-color", content: "#3eaf7c" }],
    ["meta", { name: "apple-mobile-web-app-capable", content: "yes" }],
    [
      "meta",
      { name: "apple-mobile-web-app-status-bar-style", content: "black" }
    ],
    [
      "link",
      { rel: "apple-touch-icon", href: `/icons/apple-icon-152x152.png` }
    ],
    [
      "link",
      {
        rel: "mask-icon",
        href: "/icons/safari-pinned-tab.svg",
        color: "#3eaf7c"
      }
    ],
    [
      "meta",
      {
        name: "msapplication-TileImage",
        content: "/icons/ms-icon-144x144.png"
      }
    ],
    ["meta", { name: "msapplication-TileColor", content: "#000000" }]
  ],
  theme: "thindark",
  themeConfig: {
    repo: "https://gitlab.com/autoferrit/knowledgebase",
    editLinks: true,
    docsDir: "docs",
    smoothScroll: true,
    editLinkText: "Edit this page on GitHub",
    lastUpdated: "Last Updated",
    nav: require("./nav/en"),
    sidebar: {
      "/python/": getPythonSidebar("Python", "Django")
    }
  },
  plugins: [
    ["@vuepress/back-to-top", true],
    [
      "@vuepress/pwa",
      {
        serviceWorker: true,
        updatePopup: true
      }
    ],
    ["@vuepress/medium-zoom", true],
    [
      "@vuepress/google-analytics",
      {
        ga: ""
      }
    ],
    [
      "container",
      {
        type: "vue",
        before: '<pre class="vue-container"><code>',
        after: "</code></pre>"
      }
    ],
    [
      "container",
      {
        type: "upgrade",
        before: info => `<UpgradePath title="${info}">`,
        after: "</UpgradePath>"
      }
    ],
    ["flowchart"]
  ],
  extraWatchFiles: [".vuepress/nav/en.js"]
});

function getPythonSidebar(groupA, groupB) {
  return [
    {
      title: groupA,
      collapsable: false,
      children: [["", "Learning Python"], "sys", "functional"]
    },
    {
      title: groupB,
      collapsable: false,
      children: [
        ["django", "Introduction"],
        ["django-started", "Getting started"]
      ]
    }
  ];
}
